FROM node:18.18.1
WORKDIR /app
COPY package*.json ./
RUN yarn
COPY . .
