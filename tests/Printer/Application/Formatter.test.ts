import {describe, expect} from "@jest/globals";
import {Formatter} from "../../../src/Printer/Application/Formatter";
import {EventMatchFactory} from "../../../src/Printer/Application/Factory/EventMatchFactory";
import {Serializer} from "../../../src/Printer/Application/Serializer/Serializer";
import {createMock} from "@golevelup/ts-jest";
import {FormatterOutput} from "../../../src/Printer/Application/FormatterOutput";
import {MatchUpdated} from "../../../src/Printer/Application/Event/Integration/MatchUpdated";
import {TestMatchEvent} from "../../Supports/TestMatchEvent";

describe('FormatterTest', () => {
    let serializer: Serializer;
    let factory: EventMatchFactory;
    let subjectUnderTest: Formatter;

    beforeEach(() => {
        serializer = createMock<Serializer>({
            serialize(event: MatchUpdated): FormatterOutput {
                return new FormatterOutput('win vs lose', '1:2');
            },
        });

        factory = createMock<EventMatchFactory>({
            create(event: MatchUpdated): MatchUpdated {
                return new TestMatchEvent(event.sport);
            }
        });

        subjectUnderTest = new Formatter(
            factory,
            serializer,
        );
    });

    it('should handle event and return output', () => {
        const result = subjectUnderTest.format({sport: 'xyz'});
        expect(result).toEqual({"name": "win vs lose", "score": "1:2"});
    });
});