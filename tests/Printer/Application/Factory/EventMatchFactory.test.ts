import {describe, expect} from "@jest/globals";
import {EventMatchFactory} from "../../../../src/Printer/Application/Factory/EventMatchFactory";
import {BasketballMatchUpdated} from "../../../../src/Printer/Application/Event/Integration/BasketballMatchUpdated";
import {SoccerMatchUpdated} from "../../../../src/Printer/Application/Event/Integration/SoccerMatchUpdated";
import {VolleyballMatchUpdated} from "../../../../src/Printer/Application/Event/Integration/VolleyballMatchUpdated";
import {HandballMatchUpdated} from "../../../../src/Printer/Application/Event/Integration/HandballMatchUpdated";

describe('EventMatchFactoryTest', () => {

    let subjectUnderTest: EventMatchFactory;

    beforeEach(() => {
        subjectUnderTest = new EventMatchFactory();
    });

    const testCases = [
        {
            event: {
                sport: 'basketball',
                participant1: 'GKS Tychy',
                participant2: 'GKS Katowice',
                score: [
                    ['9:7', '2:1'],
                    ['5:3', '9:9']
                ],
            },
            expectedObject: BasketballMatchUpdated,
        },
        {
            event: {
                sport: 'handball',
                participant1: 'Pogoń Szczeciń',
                participant2: 'Azoty Puławy',
                score: '34:26',
            },
            expectedObject: HandballMatchUpdated,
        },
        {
            event: {
                sport: 'volleyball',
                participant1: 'Germany',
                participant2: 'France',
                score: '3:0,25:23,25:19,25:21',
            },
            expectedObject: VolleyballMatchUpdated,
        },
        {
            event: {
                sport: 'soccer',
                participant1: 'Chelsea',
                participant2: 'Arsenal',
                score: '2:1',
            },
            expectedObject: SoccerMatchUpdated,
        },
    ];

    it.each(testCases)('Check created instance', ({event, expectedObject}) => {
        const result = subjectUnderTest.create(event);

        expect(result).toBeInstanceOf(expectedObject);
    });
});