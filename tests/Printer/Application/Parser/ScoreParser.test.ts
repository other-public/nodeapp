import {describe, expect} from "@jest/globals";
import {ScoreParser} from "../../../../src/Printer/Application/Parser/ScoreParser";

describe('ScoreParserTest', () => {
    it('should parse string to defined format with main score and sets', () => {
        const result = ScoreParser.parse('3:0,25:23,25:19,25:21');

        expect(result).toStrictEqual('Main score: 3:0 (set1 25:23, set2 25:19, set3 25:21)');
    });
});