import {describe, expect} from "@jest/globals";
import {TitleParser} from "../../../../src/Printer/Application/Parser/TitleParser";

describe('TitleParserTest', () => {
    it('should parse string to expected title with given delimiter', () => {
        const result = TitleParser.parse('TeamA ԅ(≖‿≖ԅ)', 'TeamB (⌐■_■)', '&');

        expect(result).toStrictEqual('TeamA ԅ(≖‿≖ԅ) & TeamB (⌐■_■)');
    });
});