import {describe, expect} from "@jest/globals";
import {
    BasketballAndSoccerMatchUpdatedEventSerializer
} from "../../../../../src/Printer/Application/Serializer/EventSerializer/BasketballAndSoccerMatchUpdatedEventSerializer";
import {BasketballMatchUpdated} from "../../../../../src/Printer/Application/Event/Integration/BasketballMatchUpdated";
import {FormatterOutput} from "../../../../../src/Printer/Application/FormatterOutput";
import {SoccerMatchUpdated} from "../../../../../src/Printer/Application/Event/Integration/SoccerMatchUpdated";

describe('BasketballAndSoccerMatchUpdatedEventSerializerTest', () => {
    let subjectUnderTest: BasketballAndSoccerMatchUpdatedEventSerializer;

    const testCases = [
        {
            event: BasketballMatchUpdated.createFromRawData({
                sport: 'basketball',
                participant1: 'GKS Tychy',
                participant2: 'GKS Katowice',
                score: [
                    ['9:7', '2:1'],
                    ['5:3', '9:9']
                ],
            }),
        },
        {
            event: SoccerMatchUpdated.createFromRawData({
                sport: 'soccer',
                participant1: 'Chelsea',
                participant2: 'Arsenal',
                score: '2:1',
            }),
        },
    ];

    beforeEach(() => {
        subjectUnderTest = new BasketballAndSoccerMatchUpdatedEventSerializer();
    });

    it('should serialize event', () => {
        const expectedObject = new FormatterOutput('Germany - France', '3:0,25:23,25:19,25:21');

        const event = BasketballMatchUpdated.createFromRawData({
            sport: 'volleyball',
            participant1: 'Germany',
            participant2: 'France',
            score: '3:0,25:23,25:19,25:21',
        });

        const result = subjectUnderTest.serialize(event);

        expect(expectedObject).toStrictEqual(result);
    });

    it.each(testCases)('should check supported events', ({event}) => {
        expect(subjectUnderTest.supports(event)).toBeTruthy();
    });
});