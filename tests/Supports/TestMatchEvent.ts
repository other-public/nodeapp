import {MatchUpdated} from "../../src/Printer/Application/Event/Integration/MatchUpdated";

export class TestMatchEvent implements MatchUpdated {
    constructor(public readonly sport: string) {
    }
}