CONTAINER_NAME=node_app
EXEC=docker exec -i $(CONTAINER_NAME)

up:
	docker-compose up -d
	$(EXEC) yarn

down:
	docker-compose down

start:
	$(EXEC) yarn start

test:
	echo "Loading tests...."
	$(EXEC) yarn test
