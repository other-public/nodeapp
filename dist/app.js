"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Formatter_1 = require("./Printer/Application/Formatter");
const EventMatchFactory_1 = require("./Printer/Application/Factory/EventMatchFactory");
const Serializer_1 = require("./Printer/Application/Serializer/Serializer");
const VolleyballMatchUpdatedEventSerializer_1 = require("./Printer/Application/Serializer/EventSerializer/VolleyballMatchUpdatedEventSerializer");
const BasketballAndSoccerMatchUpdatedEventSerializer_1 = require("./Printer/Application/Serializer/EventSerializer/BasketballAndSoccerMatchUpdatedEventSerializer");
const TennisMatchUpdatedEventSerializer_1 = require("./Printer/Application/Serializer/EventSerializer/TennisMatchUpdatedEventSerializer");
const HandballMatchUpdatedEventSerializer_1 = require("./Printer/Application/Serializer/EventSerializer/HandballMatchUpdatedEventSerializer");
const formatter = new Formatter_1.Formatter(new EventMatchFactory_1.EventMatchFactory(), new Serializer_1.Serializer(...[
    new VolleyballMatchUpdatedEventSerializer_1.VolleyballMatchUpdatedEventSerializer(),
    new BasketballAndSoccerMatchUpdatedEventSerializer_1.BasketballAndSoccerMatchUpdatedEventSerializer(),
    new TennisMatchUpdatedEventSerializer_1.TennisMatchUpdatedEventSerializer(),
    new HandballMatchUpdatedEventSerializer_1.HandballMatchUpdatedEventSerializer()
]));
const matches = [
    {
        sport: 'soccer',
        participant1: 'Chelsea',
        participant2: 'Arsenal',
        score: '2:1',
    },
    {
        sport: 'volleyball',
        participant1: 'Germany',
        participant2: 'France',
        score: '3:0,25:23,25:19,25:21',
    },
    {
        sport: 'handball',
        participant1: 'Pogoń Szczeciń',
        participant2: 'Azoty Puławy',
        score: '34:26',
    },
    {
        sport: 'basketball',
        participant1: 'GKS Tychy',
        participant2: 'GKS Katowice',
        score: [
            ['9:7', '2:1'],
            ['5:3', '9:9']
        ],
    },
    {
        sport: "tennis",
        participant1: 'Maria Sharapova',
        participant2: 'Serena Williams',
        score: '2:1,7:6,6:3,6:7',
    },
    {
        sport: "ski jumping",
    }
];
matches.forEach((value, index) => {
    try {
        formatter.format(value).print();
    }
    catch (e) { }
});
