"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BasketballAndSoccerMatchUpdatedEventSerializer = void 0;
const FormatterOutput_1 = require("../../FormatterOutput");
const BasketballMatchUpdated_1 = require("../../Event/Integration/BasketballMatchUpdated");
const SoccerMatchUpdated_1 = require("../../Event/Integration/SoccerMatchUpdated");
const TitleParser_1 = require("../../Parser/TitleParser");
class BasketballAndSoccerMatchUpdatedEventSerializer {
    serialize(event) {
        return new FormatterOutput_1.FormatterOutput(TitleParser_1.TitleParser.parse(event.participant1, event.participant2, '-'), event.score.toString());
    }
    supports(event) {
        return event instanceof BasketballMatchUpdated_1.BasketballMatchUpdated || event instanceof SoccerMatchUpdated_1.SoccerMatchUpdated;
    }
}
exports.BasketballAndSoccerMatchUpdatedEventSerializer = BasketballAndSoccerMatchUpdatedEventSerializer;
