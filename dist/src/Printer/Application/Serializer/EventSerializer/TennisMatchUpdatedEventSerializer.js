"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TennisMatchUpdatedEventSerializer = void 0;
const FormatterOutput_1 = require("../../FormatterOutput");
const TennisMatchUpdated_1 = require("../../Event/Integration/TennisMatchUpdated");
const TitleParser_1 = require("../../Parser/TitleParser");
const ScoreParser_1 = require("../../Parser/ScoreParser");
class TennisMatchUpdatedEventSerializer {
    serialize(event) {
        return new FormatterOutput_1.FormatterOutput(TitleParser_1.TitleParser.parse(event.participant1, event.participant2, 'vs'), ScoreParser_1.ScoreParser.parse(event.score));
    }
    supports(event) {
        return event instanceof TennisMatchUpdated_1.TennisMatchUpdated;
    }
}
exports.TennisMatchUpdatedEventSerializer = TennisMatchUpdatedEventSerializer;
