"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Serializer = void 0;
class Serializer {
    constructor(...serializers) {
        this._serializers = serializers;
    }
    serialize(event) {
        for (const serializer of this._serializers) {
            if (serializer.supports(event)) {
                return serializer.serialize(event);
            }
        }
        throw new Error(`Cannot serialize ${JSON.stringify(event)}`);
    }
}
exports.Serializer = Serializer;
