"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventMatchFactory = void 0;
const SoccerMatchUpdated_1 = require("../Event/Integration/SoccerMatchUpdated");
const BasketballMatchUpdated_1 = require("../Event/Integration/BasketballMatchUpdated");
const TennisMatchUpdated_1 = require("../Event/Integration/TennisMatchUpdated");
const VolleyballMatchUpdated_1 = require("../Event/Integration/VolleyballMatchUpdated");
const HandballMatchUpdated_1 = require("../Event/Integration/HandballMatchUpdated");
class EventMatchFactory {
    create(event) {
        switch (event.sport) {
            case 'soccer':
                return SoccerMatchUpdated_1.SoccerMatchUpdated.createFromRawData(event);
                break;
            case 'tennis':
                return TennisMatchUpdated_1.TennisMatchUpdated.createFromRawData(event);
                break;
            case 'volleyball':
                return VolleyballMatchUpdated_1.VolleyballMatchUpdated.createFromRawData(event);
                break;
            case 'handball':
                return HandballMatchUpdated_1.HandballMatchUpdated.createFromRawData(event);
                break;
            case 'basketball':
                return BasketballMatchUpdated_1.BasketballMatchUpdated.createFromRawData(event);
                break;
            default:
                throw Error(`Invalid sport [${event.sport}], cannot create event`);
        }
    }
}
exports.EventMatchFactory = EventMatchFactory;
