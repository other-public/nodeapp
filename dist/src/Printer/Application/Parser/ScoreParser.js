"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ScoreParser = void 0;
class ScoreParser {
    static parse(score) {
        //TODO add guard for numbers
        const scores = score.split(',');
        const sets = scores.map((value, index) => {
            return `set${index} ${value}`;
        });
        if (sets.length > 1) {
            sets.splice(0, 1);
        }
        return `Main score: ${scores[0]} (${sets.join(', ')})`;
    }
}
exports.ScoreParser = ScoreParser;
