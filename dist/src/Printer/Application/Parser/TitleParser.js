"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TitleParser = void 0;
class TitleParser {
    static parse(participant1, participant2, delimiter) {
        return `${participant1} ${delimiter} ${participant2}`;
    }
}
exports.TitleParser = TitleParser;
