"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FormatterOutput = void 0;
class FormatterOutput {
    constructor(name, score) {
        this.name = name;
        this.score = score;
    }
    print() {
        console.log({
            name: this.name,
            score: this.score,
        });
    }
}
exports.FormatterOutput = FormatterOutput;
