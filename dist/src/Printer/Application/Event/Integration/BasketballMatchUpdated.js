"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BasketballMatchUpdated = void 0;
class BasketballMatchUpdated {
    constructor(sport, participant1, participant2, score) {
        this.sport = sport;
        this.participant1 = participant1;
        this.participant2 = participant2;
        this.score = score;
    }
    static createFromRawData(rawData) {
        const { sport, participant1, participant2, score } = Object.assign({}, rawData);
        return new BasketballMatchUpdated(sport, participant1, participant2, score);
    }
}
exports.BasketballMatchUpdated = BasketballMatchUpdated;
