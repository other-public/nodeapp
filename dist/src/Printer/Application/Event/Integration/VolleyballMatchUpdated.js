"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VolleyballMatchUpdated = void 0;
class VolleyballMatchUpdated {
    constructor(sport, participant1, participant2, score) {
        this.sport = sport;
        this.participant1 = participant1;
        this.participant2 = participant2;
        this.score = score;
    }
    static createFromRawData(rawData) {
        const { sport, participant1, participant2, score } = Object.assign({}, rawData);
        return new VolleyballMatchUpdated(sport, participant1, participant2, score);
    }
}
exports.VolleyballMatchUpdated = VolleyballMatchUpdated;
