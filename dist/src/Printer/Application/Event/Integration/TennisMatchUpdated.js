"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TennisMatchUpdated = void 0;
class TennisMatchUpdated {
    constructor(sport, participant1, participant2, score) {
        this.sport = sport;
        this.participant1 = participant1;
        this.participant2 = participant2;
        this.score = score;
    }
    static createFromRawData(rawData) {
        const { sport, participant1, participant2, score } = Object.assign({}, rawData);
        return new TennisMatchUpdated(sport, participant1, participant2, score);
    }
}
exports.TennisMatchUpdated = TennisMatchUpdated;
