"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Formatter = void 0;
class Formatter {
    constructor(eventFactory, serializer) {
        this.eventFactory = eventFactory;
        this.serializer = serializer;
    }
    format(rawData) {
        return this.serializer.serialize(this.eventFactory.create(rawData));
    }
}
exports.Formatter = Formatter;
