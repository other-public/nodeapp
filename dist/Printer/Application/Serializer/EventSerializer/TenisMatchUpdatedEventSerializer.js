"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VolleyballMatchUpdatedEventSerializer = void 0;
const FormatterOutput_1 = require("../../FormatterOutput");
const VolleyballMatchUpdated_1 = require("../../Event/Integration/VolleyballMatchUpdated");
class VolleyballMatchUpdatedEventSerializer {
    serialize(event) {
        const scores = event.score.split(',');
        const sets = scores.map((value, index) => {
            return `set${index} ${value}`;
        });
        if (sets.length > 1) {
            sets.splice(0, 1);
        }
        return new FormatterOutput_1.FormatterOutput(event.participant1 + " - " + event.participant2, `Main score: ${scores[0]} (${sets.join(',')})`);
    }
    supports(event) {
        return event instanceof VolleyballMatchUpdated_1.VolleyballMatchUpdated;
    }
}
exports.VolleyballMatchUpdatedEventSerializer = VolleyballMatchUpdatedEventSerializer;
