"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StandardEventSerializer = void 0;
const PrinterOutput_1 = require("../../PrinterOutput");
const VolleyballMatchUpdated_1 = require("../../Event/Integration/VolleyballMatchUpdated");
class StandardEventSerializer {
    serialize(event) {
        const scores = event.score.split(',');
        const sets = scores.map((value, index) => {
            return `set${index} ${value}`;
        });
        if (sets.length > 1) {
            sets.splice(0, 1);
        }
        return new PrinterOutput_1.PrinterOutput(event.participant1 + " - " + event.participant2, `"Main score: " ${scores[0]} " ${sets.join(',')}")";`);
    }
    supports(event) {
        console.log('#');
        return event instanceof VolleyballMatchUpdated_1.VolleyballMatchUpdated;
    }
}
exports.StandardEventSerializer = StandardEventSerializer;
