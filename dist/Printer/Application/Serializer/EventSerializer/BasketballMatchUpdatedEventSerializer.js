"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BasketballMatchUpdatedEventSerializer = void 0;
const FormatterOutput_1 = require("../../FormatterOutput");
const BasketballMatchUpdated_1 = require("../../Event/Integration/BasketballMatchUpdated");
class BasketballMatchUpdatedEventSerializer {
    serialize(event) {
        return new FormatterOutput_1.FormatterOutput(event.participant1 + " - " + event.participant2, `${event.score})`);
    }
    supports(event) {
        return event instanceof BasketballMatchUpdated_1.BasketballMatchUpdated;
    }
}
exports.BasketballMatchUpdatedEventSerializer = BasketballMatchUpdatedEventSerializer;
