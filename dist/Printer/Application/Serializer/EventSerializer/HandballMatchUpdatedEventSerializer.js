"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HandballMatchUpdatedEventSerializer = void 0;
const FormatterOutput_1 = require("../../FormatterOutput");
const HandballMatchUpdated_1 = require("../../Event/Integration/HandballMatchUpdated");
const TitleParser_1 = require("../../Parser/TitleParser");
class HandballMatchUpdatedEventSerializer {
    serialize(event) {
        return new FormatterOutput_1.FormatterOutput(TitleParser_1.TitleParser.parse(event.participant1, event.participant2, 'vs'), event.score.toString());
    }
    supports(event) {
        return event instanceof HandballMatchUpdated_1.HandballMatchUpdated;
    }
}
exports.HandballMatchUpdatedEventSerializer = HandballMatchUpdatedEventSerializer;
