"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VolleyballMatchUpdatedEventSerializer = void 0;
const FormatterOutput_1 = require("../../FormatterOutput");
const VolleyballMatchUpdated_1 = require("../../Event/Integration/VolleyballMatchUpdated");
const TitleParser_1 = require("../../Parser/TitleParser");
const ScoreParser_1 = require("../../Parser/ScoreParser");
class VolleyballMatchUpdatedEventSerializer {
    serialize(event) {
        return new FormatterOutput_1.FormatterOutput(TitleParser_1.TitleParser.parse(event.participant1, event.participant2, '-'), ScoreParser_1.ScoreParser.parse(event.score));
    }
    supports(event) {
        return event instanceof VolleyballMatchUpdated_1.VolleyballMatchUpdated;
    }
}
exports.VolleyballMatchUpdatedEventSerializer = VolleyballMatchUpdatedEventSerializer;
