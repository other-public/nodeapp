"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnknownMatchUpdated = void 0;
class UnknownMatchUpdated {
    constructor(sport) {
        this.sport = sport;
    }
    static createFromRawData(rawData) {
        const { sport } = Object.assign({}, rawData);
        return new UnknownMatchUpdated(sport);
    }
}
exports.UnknownMatchUpdated = UnknownMatchUpdated;
