"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Printer = void 0;
class Printer {
    constructor(eventFactory, serializer) {
        this.eventFactory = eventFactory;
        this.serializer = serializer;
    }
    print(rawData) {
        return this.serializer.serialize(this.eventFactory.create(rawData));
    }
}
exports.Printer = Printer;
