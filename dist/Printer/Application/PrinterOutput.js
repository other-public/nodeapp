"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PrinterOutput = void 0;
class PrinterOutput {
    constructor(name, score) {
        this.name = name;
        this.score = score;
    }
    print() {
        console.log({
            name: this.name,
            score: this.score,
        });
    }
}
exports.PrinterOutput = PrinterOutput;
