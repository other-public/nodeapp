import {Formatter} from "./Printer/Application/Formatter";
import {EventMatchFactory} from "./Printer/Application/Factory/EventMatchFactory";
import {Serializer} from "./Printer/Application/Serializer/Serializer";
import {VolleyballMatchUpdatedEventSerializer} from "./Printer/Application/Serializer/EventSerializer/VolleyballMatchUpdatedEventSerializer";
import {
    BasketballAndSoccerMatchUpdatedEventSerializer
} from "./Printer/Application/Serializer/EventSerializer/BasketballAndSoccerMatchUpdatedEventSerializer";
import {
    TennisMatchUpdatedEventSerializer
} from "./Printer/Application/Serializer/EventSerializer/TennisMatchUpdatedEventSerializer";
import {
    HandballMatchUpdatedEventSerializer
} from "./Printer/Application/Serializer/EventSerializer/HandballMatchUpdatedEventSerializer";

const formatter = new Formatter(
    new EventMatchFactory(),
    new Serializer(...[
        new VolleyballMatchUpdatedEventSerializer(),
        new BasketballAndSoccerMatchUpdatedEventSerializer(),
        new TennisMatchUpdatedEventSerializer(),
        new HandballMatchUpdatedEventSerializer()
    ])
);

const matches = [
    {
        sport: 'soccer',
        participant1: 'Chelsea',
        participant2: 'Arsenal',
        score: '2:1',
    },
    {
        sport: 'volleyball',
        participant1: 'Germany',
        participant2: 'France',
        score: '3:0,25:23,25:19,25:21',
    },
    {
        sport: 'handball',
        participant1: 'Pogoń Szczeciń',
        participant2: 'Azoty Puławy',
        score: '34:26',
    },
    {
        sport: 'basketball',
        participant1: 'GKS Tychy',
        participant2: 'GKS Katowice',
        score: [
            ['9:7', '2:1'],
            ['5:3', '9:9']
        ],
    },
    {
        sport: "tennis",
        participant1: 'Maria Sharapova',
        participant2: 'Serena Williams',
        score: '2:1,7:6,6:3,6:7',
    },
    {
        sport: "ski jumping",
    }
];

matches.forEach((value, index) => {
    try {
        formatter.format(value).print();
    } catch (e) {}
})
