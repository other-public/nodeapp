import {MatchUpdated} from "./MatchUpdated";

export class UnknownMatchUpdated implements MatchUpdated {
    constructor(
        public readonly sport: string,
    ) {
    }

    public static createFromRawData(rawData: object): UnknownMatchUpdated
    {
        const { sport } = Object.assign({} as any, rawData);
        return new UnknownMatchUpdated(sport);
    }
}