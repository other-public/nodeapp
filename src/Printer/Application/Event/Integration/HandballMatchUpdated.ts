import {MatchUpdated} from "./MatchUpdated";

export class HandballMatchUpdated implements MatchUpdated {
    constructor(
        public readonly sport: string,
        public readonly participant1: string,
        public readonly participant2: string,
        public readonly score: string,
    ) {
    }

    public static createFromRawData(rawData: object): HandballMatchUpdated
    {
        const { sport, participant1, participant2, score } = Object.assign({} as any, rawData);
        return new HandballMatchUpdated(sport, participant1, participant2, score);
    }
}