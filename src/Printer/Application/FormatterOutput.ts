export class FormatterOutput {
    constructor(
        public readonly name: string,
        public readonly score: string,
    ) {
    }

    public print(): void {
        console.log({
            name: this.name,
            score: this.score,
        });
    }
}
