export class ScoreParser {
    public static parse(score: string) {
        //TODO add guard for numbers
        const scores = score.split(',');

        const sets = scores.map((value, index) => {
            return `set${index} ${value}`;
        });

        if (sets.length > 1) {
            sets.splice(0, 1);
        }

        return `Main score: ${scores[0]} (${sets.join(', ')})`;
    }
}