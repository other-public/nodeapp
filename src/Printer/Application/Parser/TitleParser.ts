export class TitleParser {
    public static parse(participant1: string, participant2: string, delimiter: string) {
        return `${participant1} ${delimiter} ${participant2}`;
    }
}
