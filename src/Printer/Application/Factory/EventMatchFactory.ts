import {MatchUpdated} from "../Event/Integration/MatchUpdated";
import {SoccerMatchUpdated} from "../Event/Integration/SoccerMatchUpdated";
import {BasketballMatchUpdated} from "../Event/Integration/BasketballMatchUpdated";
import {TennisMatchUpdated} from "../Event/Integration/TennisMatchUpdated";
import {VolleyballMatchUpdated} from "../Event/Integration/VolleyballMatchUpdated";
import {HandballMatchUpdated} from "../Event/Integration/HandballMatchUpdated";

export class EventMatchFactory {
    public create(event: MatchUpdated): MatchUpdated
    {
        switch (event.sport) {
            case 'soccer':
                return SoccerMatchUpdated.createFromRawData(event);
                break;
            case 'tennis':
                return TennisMatchUpdated.createFromRawData(event);
                break;
            case 'volleyball':
                return VolleyballMatchUpdated.createFromRawData(event);
                break;
            case 'handball':
                return HandballMatchUpdated.createFromRawData(event);
                break;
            case 'basketball':
                return BasketballMatchUpdated.createFromRawData(event);
                break;
            default:
                throw Error(`Invalid sport [${event.sport}], cannot create event`);
        }
    }
}