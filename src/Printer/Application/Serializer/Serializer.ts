import {EventSerializer} from "./EventSerializer";
import {FormatterOutput} from "../FormatterOutput";
import {MatchUpdated} from "../Event/Integration/MatchUpdated";

export class Serializer {
    private _serializers: EventSerializer[];

    constructor(...serializers: EventSerializer[]) {
        this._serializers = serializers;
    }

    public serialize(event: MatchUpdated): FormatterOutput
    {
        for (const serializer of this._serializers) {
            if (serializer.supports(event)) {
                return serializer.serialize(event);
            }
        }

        throw new Error(`Cannot serialize ${JSON.stringify(event)}`);
    }
}
