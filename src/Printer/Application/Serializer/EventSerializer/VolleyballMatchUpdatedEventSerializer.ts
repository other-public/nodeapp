import {EventSerializer} from "../EventSerializer";
import {MatchUpdated} from "../../Event/Integration/MatchUpdated";
import {FormatterOutput} from "../../FormatterOutput";
import {VolleyballMatchUpdated} from "../../Event/Integration/VolleyballMatchUpdated";
import {TitleParser} from "../../Parser/TitleParser";
import {ScoreParser} from "../../Parser/ScoreParser";

export class VolleyballMatchUpdatedEventSerializer implements EventSerializer {
    serialize(event: VolleyballMatchUpdated): FormatterOutput {
        return new FormatterOutput(
            TitleParser.parse(event.participant1, event.participant2, '-'),
            ScoreParser.parse(event.score),
        );
    }

    supports(event: MatchUpdated): boolean {
        return event instanceof VolleyballMatchUpdated;
    }
}
