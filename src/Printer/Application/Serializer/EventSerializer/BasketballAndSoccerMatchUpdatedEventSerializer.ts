import {EventSerializer} from "../EventSerializer";
import {MatchUpdated} from "../../Event/Integration/MatchUpdated";
import {FormatterOutput} from "../../FormatterOutput";
import {BasketballMatchUpdated} from "../../Event/Integration/BasketballMatchUpdated";
import {SoccerMatchUpdated} from "../../Event/Integration/SoccerMatchUpdated";
import {TitleParser} from "../../Parser/TitleParser";

export class BasketballAndSoccerMatchUpdatedEventSerializer implements EventSerializer {
    serialize(event: BasketballMatchUpdated): FormatterOutput {
        return new FormatterOutput(
            TitleParser.parse(event.participant1, event.participant2, '-'),
            event.score.toString(),
        );
    }

    supports(event: MatchUpdated): boolean {
        return event instanceof BasketballMatchUpdated || event instanceof SoccerMatchUpdated;
    }
}