import {EventSerializer} from "../EventSerializer";
import {MatchUpdated} from "../../Event/Integration/MatchUpdated";
import {FormatterOutput} from "../../FormatterOutput";
import {HandballMatchUpdated} from "../../Event/Integration/HandballMatchUpdated";
import {TitleParser} from "../../Parser/TitleParser";

export class HandballMatchUpdatedEventSerializer implements EventSerializer {
    serialize(event: HandballMatchUpdated): FormatterOutput {
        return new FormatterOutput(
            TitleParser.parse(event.participant1, event.participant2, 'vs'),
            event.score.toString(),
        );
    }

    supports(event: MatchUpdated): boolean {
        return event instanceof HandballMatchUpdated;
    }
}