import {EventSerializer} from "../EventSerializer";
import {MatchUpdated} from "../../Event/Integration/MatchUpdated";
import {FormatterOutput} from "../../FormatterOutput";
import {TennisMatchUpdated} from "../../Event/Integration/TennisMatchUpdated";
import {TitleParser} from "../../Parser/TitleParser";
import {ScoreParser} from "../../Parser/ScoreParser";

export class TennisMatchUpdatedEventSerializer implements EventSerializer {
    serialize(event: TennisMatchUpdated): FormatterOutput {
        return new FormatterOutput(
            TitleParser.parse(event.participant1, event.participant2, 'vs'),
            ScoreParser.parse(event.score),
        );
    }

    supports(event: MatchUpdated): boolean {
        return event instanceof TennisMatchUpdated;
    }
}