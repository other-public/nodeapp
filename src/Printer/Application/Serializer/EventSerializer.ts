import {MatchUpdated} from "../Event/Integration/MatchUpdated";
import {FormatterOutput} from "../FormatterOutput";

export interface EventSerializer {
    serialize(event: MatchUpdated): FormatterOutput;

    supports(event: MatchUpdated): boolean;
}
