import {FormatterOutput} from "./FormatterOutput";
import {EventMatchFactory} from "./Factory/EventMatchFactory";
import {MatchUpdated} from "./Event/Integration/MatchUpdated";
import {Serializer} from "./Serializer/Serializer";

export class Formatter {
    constructor(
        private eventFactory: EventMatchFactory,
        private serializer: Serializer,
    ) {
    }

    public format(rawData: object): FormatterOutput
    {
        return this.serializer.serialize(this.eventFactory.create(rawData as MatchUpdated));
    }
}
