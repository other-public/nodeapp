# Node APP

### Technologies & Tools
* NodeJS 18.18.1 (LTS)
* Docker
* Docker compose
* Typescript

### Description

#### How to start
1. `make up` - docker will build image with node `18.18.1` (required for this project - locked in package.json)
2. `make start` - run application
3. `make test` -  run tests
4. `make down` -  kill docker

#### Improvements area
* `EventMatchFactory` can be implemented as `Abstract Factory (design pattern, not abstract class)` and load many factories (Factory method) to create object injected by DI (framework feature) 
* `Layered architecture (Onion)` is over-engineering for this simple app, but I want to show where I will have to put events (integration events)
* Tests - Unit tests can be more strict to check `expected value` instead check only `instances` in particular cases
* Missed integration test for `Formatter` class
* .tslinter
* `Parser classes` they can be injectable instead of static

#### Used design patterns
* Strategy
* Factory
* Facade
* Composite
